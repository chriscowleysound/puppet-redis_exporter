# redis_exporter::service
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include redis_exporter::service
class redis_exporter::service {
  include systemd
  file {'/etc/systemd/system/redis_exporter.service':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('redis_exporter/redis_exporter.systemd.epp', {
        'bin_dir' => "/opt/${redis_exporter::archive_name}"
      }
    )
  }
}
