# redis_exporter
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include redis_exporter
class redis_exporter (
  $verson      = $::redis_exporter::params::version,
  $real_archive_url = $::redis_exporter::params::real_archive_url,
  $archive_name = $::redis_exporter::params::archive_name,
  $local_archive_name = $::redis_exporter::params::local_archive_name,
  $manage_user      = $::redis_exporter::params::manage_user,
  $manage_group     = $::redis_exporter::params::manage_group,
) inherits redis_exporter::params {
  class {'::redis_exporter::install': }
  ->class { '::redis_exporter::config': }
  ~>class { '::redis_exporter::service': }
  ->Class['::redis_exporter']
}
