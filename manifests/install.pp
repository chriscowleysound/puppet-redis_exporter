# redis_exporter::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include redis_exporter::install
class redis_exporter::install {
  include archive

  if $redis_exporter::manage_user {
    ensure_resource('user', ['redis_exporter'], {
      ensure => 'present',
      system => true,
    })
  }
  if $redis_exporter::manage_group {
    ensure_resource('group', ['redis_exporter'], {
      ensure => 'present',
      system => true,
    })
    Group['redis_exporter']->User['redis_exporter']
  }
  archive { "/tmp/${redis_exporter::local_archive_name}":
    ensure          => present,
    extract         => true,
    extract_path    => '/opt',
    source          => $redis_exporter::real_archive_url,
    checksum_verify => false,
    creates         => "/opt/${redis_exporter::archive_name}/rabbitmq_exporter",
    cleanup         => true,
  }
}
