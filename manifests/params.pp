# redis_exporter::params
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include redis_exporter::params
class redis_exporter::params {
  $version          = '0.12'
  $manage_user      = true
  $manage_group     = true
  $archive_url_base = 'https://github.com/oliver006/redis_exporter/releases/download/'
  $archive_name     = 'redis_exporter'
  $archive_url      = undef

  case $::architecture {
    'x86_64', 'amd64': { $arch = 'amd64' }
    'i386':            { $arch = '386' }
    default: {
      fail("${::architecture} unsuported")
    }
  }
  case $::kernel {
    'Linux': { $os = downcase($::kernel)}
    default: {
      fail("${::kernel} not supported")
    }
  }
  case $::osfamily {
    'Debian': { $conf_file = '/etc/default/rabbitmq_exporter' }
    'RedHat': { $conf_file = '/etc/sysconfig/rabbitmq_exporter' }
    default: {
      fail("${::operatingsystem} not supported")
    }
  }
  $real_archive_url = pick(
    $archive_url,
    "${archive_url_base}/v${version}/${archive_name}-${version}.${os}-${arch}.tar.gz"
  )
  $local_archive_name = "${archive_name}-${version}.${os}-${arch}.tar.gz"
}
